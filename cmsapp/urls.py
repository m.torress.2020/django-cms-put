from django.urls import path
from . import views

urlpatterns = [
    path("<llave>", views.get_content),
    path("", views.index)
]