from django.shortcuts import render
from django.http import HttpResponse
from .models import Content
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == 'PUT':
        value = request.body.decode('utf-8')
        c = Content(key=llave, value=value)
        c.save()
        #response = "data saved"
        try:
            fila = Content.objects.get(key=llave)
            response = fila.value
        except Content.DoesNotExist:
            response = "no data found"

    elif request.method == 'GET':
        fila = Content.objects.get(key=llave)
        response = fila.value

    elif request.method == 'POST':
        value = request.body.decode('utf-8')
        fila = Content.objects.get(key=llave)
        fila.value = value
        fila.save()
        response = "data updated"

    return HttpResponse(response)


def index(request):
    content_list = Content.objects.all()[:5]
    content = {'content_list', content_list}
    return render(request, 'cmsapp/index.html', content)